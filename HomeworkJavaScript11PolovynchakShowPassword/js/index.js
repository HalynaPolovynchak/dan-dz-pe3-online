"use strict";

let showPassword = document.querySelectorAll(".icon-password");
showPassword.forEach(item =>
    item.addEventListener("click", toggleType));

function toggleType(e){
    let input = e.target.closest(".input-wrapper").querySelector(".password");
    if (input.type === "password") {
        input.type = "text";
        this.classList.remove("fa-eye");
        this.classList.add("fa-eye-slash");
    } else {
        input.type = "password";
        this.classList.remove("fa-eye-slash");
        this.classList.add("fa-eye");
    }
}

const createError = (msg) => {
    root.append(msg);
    setTimeout(() => root.innerHTML="",2000);
};
const checkPassword = () => {
    const inputArr = document.getElementsByTagName("input");
    const password1 = inputArr[0].value;
    const password2 = inputArr[1].value;
    if ( password1 && password1 === password2) alert("You are welcome"); //password1 или password2 любое из полей должно быть введено, чтоб пройти или не пройти второе условие
    else if(!password1) createError("Пароль не введен");
    else createError("Нужно ввести одинаковые значения");
};

document.querySelector(".btn").addEventListener("click", checkPassword);