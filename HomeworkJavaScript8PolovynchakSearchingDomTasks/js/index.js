"use strict";
//1
let arrParagraph = document.querySelectorAll('p');
for (let elem of arrParagraph) {
    elem.style.background = '#ff0000'
}
//2
let elemOptionList = document.getElementById('optionsList');
console.log(elemOptionList);

let elemOptionListParent = elemOptionList.parentElement;

console.log(elemOptionListParent);
if (elemOptionList.hasChildNodes()) {
    let elemOptionListNodes = elemOptionList.childNodes;
    for (let elem of elemOptionListNodes) {
    console.log(elem, typeof elem);
    }
}
//3
let testParagraph = document.getElementById('testParagraph');
testParagraph.textContent = "This is a paragraph";

//4-5
let mainHeaderList = document.getElementsByClassName('main-header');
for (let elem of mainHeaderList) {
    let child = elem.children;
    for (let elem of child) {
        elem.classList.add('.nav-item');
        console.log(elem);
    }
}

//6
// const delClassList = document.querySelectorAll('.section-title');
// не находит элементов с таким классом, нечего удалять
// решила попробовать с другим классом options-list-item на предмет работы
const delClassList = document.querySelectorAll('.options-list-item');
for (let elem of delClassList) {
    elem.classList.remove('options-list-item');
    // console.log(elem); // проверка
}
