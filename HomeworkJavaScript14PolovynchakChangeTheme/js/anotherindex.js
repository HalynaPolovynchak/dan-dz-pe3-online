"use strict";

document.querySelector('.theme_toggle').addEventListener("click", (event)=>{
event.preventDefault();
if(localStorage.getItem('theme') === 'dark') {
    localStorage.removeItem('theme');
} else {
    localStorage.setItem('theme', 'dark');
}
    addDarkHTML();
})

function addDarkHTML(){
    try {
        if(localStorage.getItem('theme') === 'dark') {
            document.querySelector('html').classList.add('dark');
            document.querySelector('.theme_toggle span').textContent = 'dark_mode';
        }
        else {
            document.querySelector('html').classList.remove('dark');
            document.querySelector('.theme_toggle span').textContent = 'wb_sunny';
        }
    } catch (err) {}
}

addDarkHTML();


