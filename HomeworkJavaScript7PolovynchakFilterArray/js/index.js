"use strict";

let array = [8, "cat", "", 41, "apple", true, 25, null,`change`, undefined, "dog", false, `65`, 0, NaN];

function filterBy (array, dataType='string') {
    let newArrayChecked = array.filter((el) => typeof el !== dataType);
    console.log(newArrayChecked);
    return newArrayChecked;
}
filterBy(array, 'string');
filterBy(array, 'number');
filterBy(array, 'boolean');
filterBy(array, 'null');
filterBy(array);

// let removedItems = array.splice(0);
// console.log(array);