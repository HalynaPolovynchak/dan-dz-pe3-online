"use strict";

const speedArray = [12,8,25,15,17,11,21,10,23,18];
const taskArray = [328,629,245,112,589,450,212,1854];
const deadline = new Date('September 07, 2022 00:00:00');

function sprintPlanning(speed, task, date){
    let todayDate = new Date();
    const balanceTime = (date - todayDate)/1000/60/60/(24/8)/7*5; //кол-во доступных рабочих часов

    let teamSpeedDay = 0;
    for (let i=0; i < speed.length; i++) {
        teamSpeedDay = teamSpeedDay + speed[i];
    }

    let taskVolume = 0;
    for (let i=0; i < task.length; i++) {
        taskVolume = taskVolume + task[i];
    }

    // console.log((taskVolume/teamSpeedDay)*8); // кол-во часов требуемое на выполнение заданий
    if ((taskVolume/teamSpeedDay)*8 <= balanceTime){
        document.querySelector('.alert_text').classList.add('active');
        document.querySelector('.alert_text').textContent = 'Усі завдання будуть успішно виконані за '
            + `${Math.floor(balanceTime/8-taskVolume/teamSpeedDay)}` + ' днів до настання дедлайну!';
    } else {
        document.querySelector('.alert_text').classList.add('active');
        document.querySelector('.alert_text').textContent = 'Команді розробників доведеться витратити додатково '
            + `${Math.ceil(taskVolume/teamSpeedDay*8 - balanceTime)}`
            + ' годин після дедлайну, щоб виконати всі завдання в беклозі';
    }
}

sprintPlanning(speedArray, taskArray, deadline);