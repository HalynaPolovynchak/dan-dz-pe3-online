"use strict";
// Завдання
let userNumber;
let count = 0;

do {
    userNumber = +prompt("Enter your number, please", userNumber);
} while ((Number.isInteger(userNumber)) === false)
for (let number = 0; number <= userNumber; number++) {
    if (number % 5 === 0) {
        count += 1;
        console.log(number);
    }
}
if (count < 1) {
    console.log("Sorry, no numbers");
}

// if (userNumber < 5) {
//     console.log("Sorry, no numbers");
// }

// Необов'язкове завдання підвищеної складності

let number1;
let number2;
let m, n;

do {
    number1 = +prompt("Enter your first number, please", number1);
    number2 = +prompt("Enter your second number, please", number2);
    if (((Number.isInteger(number1)) && (Number.isInteger(number2))) === false) {
        alert("Sorry, incorrect number! Числа мають бути цілими");
    }
} while (((Number.isInteger(number1)) && (Number.isInteger(number2))) === false)
if (number1>number2) {
    m = number2;
    n = number1;
} else {
    m = number1;
    n = number2;
}
// Перевірка умови меньшості
// console.log('Меньше = ' + m);
// console.log('Більше = ' + n);

for (let checkNumber = m ; checkNumber <= n; checkNumber++) {
    let primeNumCount = 0;
    for (let numDivider = 1; numDivider <= checkNumber; numDivider++) {
        if (Number.isInteger(checkNumber/numDivider)) {
            primeNumCount += 1;
        }
    }
    if (primeNumCount<=2) {
        console.log(checkNumber);
    }
}
