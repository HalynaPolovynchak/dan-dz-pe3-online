const { parallel, series, watch, src, dest } = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass')(require('sass'));
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');

const serv = (done) => {
    browserSync.init({
        server: {
            baseDir: './',
        },
        notify: false,
        online: true,
        browser: 'chrome',
        open: true,
    });
};

const scripts = (cb) => {
    src('./src/js/**/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(dest('./src/js'))
        .pipe(uglify())
        .pipe(dest('./dist/js'))
        .pipe(browserSync.stream());
    cb();
};

const styles = (cb) => {
    src('./src/styles/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(dest('./src/css'))
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 version'], grid: true }))
        .pipe(cleanCSS(( { level: { 1:{ specialComments: 0 } } } )))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream());
    cb();
};

// const minCleanCss = (cb) => {
//     src("./src/styles/**/*.css")
//         .pipe(concat('styles.min.css'))
//         .pipe(cleanCSS())
//         .pipe(dest("./dist/css/"))
//         .pipe(browserSync.stream());
//     cb();
// };

const images = (cb) => {
    src('./src/img/**/*.{jpg,jpeg,png,svg,webp}')
        .pipe(imagemin())
        .pipe(dest('./dist/img'));
    cb();
}

const watcher = () => {
    watch('*.html').on('change', browserSync.reload);
    watch(['./src/js/**/*.js', '!./src/js/**/*min.js'])
        .on('change', series(scripts, browserSync.reload));
    watch('./src/styles/**/*.scss', styles);
    watch('./src/img/**/*.{jpg,jpeg,png,svg,webp}', images);
};

const cleanDist = (cb) => {
    src('dist', {allowEmpty: true})
        .pipe(clean());
    cb();
};

function buildCopy() {
    return src([
        'src/css/**/*.min.css',
        'src/js/**/*.min.js',
        'src/img/dest/**/*',
    ], { base: 'src' }) // Параметр "base" сохраняет структуру проекта при копировании
        .pipe(dest('dist'))
}

// exports.serv = serv;
// exports.scripts = scripts;
// exports.styles = styles;
// exports.watcher = watcher;
// exports.images = images;
// exports.cleanDist = cleanDist;
// exports.buildcopy = buildcopy;
exports.dev = parallel(serv, watcher, series(styles, scripts));
exports.build = parallel(cleanDist, series(styles, scripts, images));
exports.build = series(cleanDist, styles, scripts, images, buildCopy);


