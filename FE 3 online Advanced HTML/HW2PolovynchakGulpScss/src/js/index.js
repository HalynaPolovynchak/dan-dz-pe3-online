'use strict'

document.querySelector('.header__burger')
    .addEventListener("click", (event) => {
    document.querySelector(".header__burger").classList.toggle("header__burger--active");
    document.querySelector(".header__menu").classList.toggle("header__menu--active");
    // document.querySelector("body").classList.toggle("body--lock");
})

function headerMenuActive () {
    if (window.innerWidth >= 768){
        document.querySelector(".header__burger").classList.remove("header__burger--active");
        document.querySelector(".header__menu").classList.remove("header__menu--active");
    }
}

window.addEventListener("resize", headerMenuActive);