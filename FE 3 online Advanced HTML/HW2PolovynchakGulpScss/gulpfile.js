const { parallel, series, watch, src, dest } = require('gulp');
// const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass')(require('sass'));
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');

function serv() {
    browserSync.init({
        server: { baseDir: './' },
        notify: false,
        online: true
    })
}

const scripts = (cb) => {
    src(['./src/js/**/*.js', '!./src/js/**/*min.js'])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(dest('./src/js'))
        .pipe(dest('./dist/js'))
        .pipe(browserSync.stream());
    cb();
};

const styles = (cb) => {
    src('./src/styles/style.scss')
        .pipe(sass())
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 version'], grid: true }))
        .pipe(cleanCSS(( { level: { 1:{ specialComments: 0 } } } )))
        .pipe(dest('./src/css'))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream());
    cb();
};

function images(){
    return src('./src/img/**/*.{jpg,jpeg,png,svg,webp}')
        .pipe(imagemin())
        // .pipe(dest('./src/img/dest'))
        .pipe(dest('./dist/img'));
};

// function cleanImg() {
//     return src('./src/img/dest/', {allowEmpty: true})
//         .pipe(clean());
// }

const watcher = () => {
    watch('*.html').on('change', browserSync.reload);
    watch(['./src/js/**/*.js', '!./src/js/**/*min.js'])
        .on('change', series(scripts, browserSync.reload));
    watch('./src/styles/**/*.scss', styles);
    watch('./src/img/**/*.{jpg,jpeg,png,svg,webp}', images);
};

const cleanDist = (cb) => {
    src('./dist', {allowEmpty: true})
        .pipe(clean());
    cb();
};

const buildCopy = (cb) => {
    src([
        './src/css/**/*.min.css',
        './src/js/**/*.min.js',
        './src/img/**/*',
    ], { base: './src' })
        .pipe(dest('./dist'));
    cb();
};

exports.serv = serv;
exports.scripts = scripts;
exports.styles = styles;
exports.watcher = watcher;
exports.images = images;
// exports.cleanImg = cleanImg;
exports.cleanDist = cleanDist;
exports.buildCopy = buildCopy;


exports.dev = series(images, styles, scripts, parallel(serv, watcher));
exports.build = series(cleanDist, styles, scripts, images, buildCopy);
// exports.default = parallel(serv, watcher, series(cleanDist, cleanImg, styles, scripts, images, buildCopy));
