"use strict";

let num1;
let num2;
let symb;

do {
    num1 = +prompt("Enter 1 number", num1);
    num2 = +prompt("Enter 2 number", num2);
    symb = prompt("Enter operation: + , - , / , * , ** ");
}
while (!(num1 && !isNaN(num1) && typeof +num1 === "number" &&
        num2 && !isNaN(num2) && typeof +num2 === "number")
        );

function result (number1, number2, symbolOper) {
let res;
number1 = num1;
number2 = num2;
symbolOper = symb;

    switch (symbolOper) {
        case "+":
            res = number1 + number2;
            break;
        case "-":
            res = number1 - number2;
            break;
        case "/":
            res = number1 / number2;
            break;
        case "*":
            res = number1 * number2;
            break;
        case "**":
            res = number1 ** number2;
            break;
        default:
            break;
    }
    return res;
}

console.log(result(num1, num2, symb));