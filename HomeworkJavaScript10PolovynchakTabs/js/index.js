"use strict";

let tab = function (){
    let tabNav = document.querySelectorAll(".tabs-title"); //сделали массив из навигационного меню
    let tabContent = document.querySelectorAll(".tab"); //получили массив контентов
    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav)
    });
    function selectTabNav(){
        let tabName; //доп переменая определения названия вкладки для вызова соответствующего контента
        tabNav.forEach(item => {
            item.classList.remove("active");
        })
        this.classList.add("active");
        tabName = this.getAttribute("data-tab-name");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item => {
            item.classList.contains(tabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        })
    }
};
tab();

