"use strict";

let userName;
let userAge;

do {
    userName = prompt("Enter Your Name", userName);
    userAge = +prompt("Enter Your Age", userAge);
} while (!userName ||
        !(userAge && userName && userAge !==0 && !isNaN(userAge) && typeof +userAge === "number") ||
        userName === "" ||
        userName === null);
if (userAge < 18) {
    alert("You are not allowed to visit this website")
} else if (userAge >= 18 && userAge <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert("Welcome, " + userName)
    } else {
        alert("You are not allowed to visit this website")
    }
} else {
    alert("Welcome, " + userName)
}
