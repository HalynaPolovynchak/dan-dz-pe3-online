"use strict";

const inputText = `Price, $: `;
root.append(inputText);
root.style.cssText = `background-color: orange; width: 250px; height: auto; padding: 5px 5px;`;

let inputPrice = document.createElement("input");
root.append(inputPrice);

let btnX, rootSpan, priceText, errorTx;

inputPrice.addEventListener("focus", onFocusInput);
function onFocusInput () {
    inputPrice.style.border = "5px solid green";
    inputPrice.value = "";
}

inputPrice.onblur = function() {
    let price = inputPrice.value;
    if (!(price && price > 0 && !isNaN(+price) && typeof +price === "number")) {
        errorTx = document.createElement("div");
        errorTx.innerHTML = 'Please enter correct price';
        inputPrice.style.border = "5px solid red";
        root.append(errorTx);
        inputPrice.addEventListener("focus", clearErrorTx);
    } else {
        rootSpan = document.createElement("div");
        priceText = document.createElement("span");
        btnX = document.createElement("button");
        btnX.textContent = "x";
        btnX.style.cssText = "border-radius: 12px; margin: 4px 3px; border: none;"
        priceText.innerText = `Текущая цена: ${inputPrice.value} $`;
        inputPrice.style.cssText = "border: 1px solid black; color: green;"
        root.prepend(rootSpan);
        rootSpan.prepend(priceText);
        rootSpan.append(btnX);
        // btnX.addEventListener("click", clearPriceText);
        // btnX.addEventListener("click", clearInputPrice);
        btnX.addEventListener("click", onFocusInputSec);
        inputPrice.addEventListener("focus", onFocusInputSec);
    }
    function clearPriceText(){
        return rootSpan.innerHTML = "";
    }
    function clearInputPrice(){
        return inputPrice.value = "";
    }
    function onFocusInputSec(){
            clearPriceText();
            clearInputPrice();
    }
    function clearErrorTx(){
        return errorTx.textContent = "";
    }
};

