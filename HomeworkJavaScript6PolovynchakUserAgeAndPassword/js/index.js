"use strict";

function createNewUser () {
    let name = prompt("Enter your first name");
    let surName = prompt("Enter your second name");
    let birthday = prompt("Enter your date of birth dd.mm.yyyy");

    let newUser = {
        firstName: name,
        lastName: surName,
        dateBirth: birthday,
        getLogin: function () {
            let login;
            login = this.firstName.toLowerCase().substr(0,1) + this.lastName.toLowerCase();
            return login;
        },
        getAge: function () {
            let dateBirthConvers = new Date(`${this.dateBirth.substr(6, 4)}-${this.dateBirth.substr(3, 2)}-${this.dateBirth.substr(0, 2)}`);
            let fullAge = Math.trunc((Date.now() - dateBirthConvers)/(1000 * 60 * 60 * 24 * 365.25));
            return fullAge;
        },
        getPassword: function () {
            let password;
            password = this.firstName.toUpperCase().substr(0,1) + this.lastName.toLowerCase() + this.dateBirth.substr(6, 4);
            return password;
        },
    };
    console.log(`Login: ${newUser.getLogin()}`);
    console.log(`User Age: ${newUser.getAge()}`);
    console.log(`Password: ${newUser.getPassword()}`);
}
createNewUser();


// Check method calculation
// const dateToday = new Date();
// console.log(dateToday, typeof dateToday);
// console.log(Date.now());


// const dateBirth = new Date("1984-05-25");
// console.log(dateBirth);
// console.log((Date.now() - dateBirth)/(1000 * 60 * 60 * 24 * 365.25));
// console.log(Math.trunc((Date.now() - dateBirth)/(1000 * 60 * 60 * 24 * 365.25)));