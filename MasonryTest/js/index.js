"use strict";

// <------------- SECTION__SERVICES ---------------->

let services = function (){
    let tabNav = document.querySelectorAll(".tabs__title");
    let tabContent = document.querySelectorAll(".tab");
    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav)
    });
    function selectTabNav(){
        let tabName;
        tabNav.forEach(item => {
            item.classList.remove("active");
        })
        this.classList.add("active");
        tabName = this.getAttribute("data-tab-name");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item => {
            item.classList.contains(tabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        })
    }
};
services();


// <------------- SECTION__WORK ---------------->

let tabWork = function (){
    let tabNav = document.querySelectorAll(".tabs__title__work");
    let tabContent = document.querySelectorAll(".tab__img");
    let tabNameChoice;

    tabNav.forEach(item => {
        if (item.classList.contains("active_img")) {
            tabNameChoice = item.getAttribute("data-tab-name");
        }

    })

    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav);
    });

    function selectTabNav(){
        let tabName;
        tabNav.forEach(item => {
            item.classList.remove("active_img");
        })
        this.classList.add("active_img");
        tabName = this.getAttribute("data-tab-name");
        tabNameChoice = tabName;
        selectTabContent(tabName);
    }

    function selectTabContent(tabName){
        tabContent.forEach(item => {
            item.classList.contains(tabName)?
                item.classList.add("active_img"):
                item.classList.remove("active_img");
        })
    }

    const btnWork = document.getElementById("button__work");
    const activeImgGrid = document.querySelector(".working__img");
    btnWork.addEventListener("click", showAddWorkImage);
    function showAddWorkImage(){
        tabContent.forEach(item => {
            if (item.classList.contains("img__hide")) {
                item.classList.add("active_img");
                item.classList.remove("img__hide");
            }
        })
        let workContainer = document.querySelector(".section__work");
        workContainer.classList.add("active_section_work");
        activeImgGrid.classList.add("active__img_grid");
        btnWork.innerHTML = "";
        selectTabContent(tabNameChoice);
    }
};
tabWork();


// <------------- SECTION__PEOPLE ---------------->

const rightBtn = document.getElementById("right_button");
rightBtn.addEventListener("click", sliderLeft);

const leftBtn = document.getElementById("left_button");
leftBtn.addEventListener("click", sliderRight);

let slidesPersons = [...document.querySelectorAll(".person__photo")];
let stepClickBtn = 0;
let activeIndex = 0;

function sliderLeft(){
    let appendChildElem = slidesPersons[stepClickBtn];
    document.querySelector('#sliderMini').appendChild(appendChildElem);

    if ((stepClickBtn + 1) === slidesPersons.length) {
        stepClickBtn = 0;
    } else {
        stepClickBtn++;
    }

    const photoActive = slidesPersons.find((item, index) => {
        const isActive = item.classList.contains("active");
        if (!isActive) return ;
        activeIndex = index;
        return item;
    })
    photoActive.classList.remove("active");

    if(activeIndex + 1 < slidesPersons.length){
        slidesPersons[activeIndex + 1].classList.add("active");
        findActiveContent(slidesPersons[activeIndex + 1]);
    } else {
        slidesPersons[activeIndex-(slidesPersons.length-1)].classList.add("active");
        findActiveContent(slidesPersons[activeIndex-(slidesPersons.length-1)]);
    }
}

function sliderRight() {
    let removeChild;
    if (stepClickBtn === 0){
        removeChild = slidesPersons[slidesPersons.length - 1];
    } else {
        removeChild = slidesPersons[stepClickBtn - 1];
    }

    if(stepClickBtn-1 === -1){
        stepClickBtn = slidesPersons.length - 1;
    } else {
        stepClickBtn--;
    }

    document.querySelector('#sliderMini').removeChild(removeChild);
    document.querySelector('#sliderMini').insertAdjacentElement("afterbegin", removeChild);

    const photoActive = slidesPersons.find((item, index) => {
        const isActive = item.classList.contains("active");
        if (!isActive) return ;
        activeIndex = index;
        return item;
    })
    photoActive.classList.remove("active");

    if (activeIndex - 1 === -1) {
        slidesPersons[slidesPersons.length - 1].classList.add("active");
        findActiveContent(slidesPersons[slidesPersons.length - 1]);
    } else {
        slidesPersons[activeIndex - 1].classList.add("active");
        findActiveContent(slidesPersons[activeIndex - 1]);
    }
}


//function for choise content in container

function findActiveContent(paramIncoming){
    let personPhotoNav = document.querySelectorAll(".person__photo");
    let peopleQuotePersonal = document.querySelectorAll(".people__quote_personal");
    let personName = document.querySelectorAll(".person__name");
    let personJobTitle = document.querySelectorAll(".person__job_title");
    let photoImgQuote = document.querySelectorAll(".photo_img_quote");

    selectQuoteTab();
    function selectQuoteTab(){
        let quoteTabName = null;

        personPhotoNav.forEach((item)=>{
            item.classList.remove("active");
        })
        paramIncoming.classList.add("active");
        quoteTabName = paramIncoming.getAttribute("data-tab-name");
        selectQuoteContent(quoteTabName);
    }

    function selectQuoteContent(quoteTabName){
        peopleQuotePersonal.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        personName.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        personJobTitle.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        photoImgQuote.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
    }
}


// personality ();
// function personality (){
//     slidesPersons = [...document.querySelectorAll(".person__photo")];
//     let personPhotoNav = document.querySelectorAll(".person__photo");
//     personPhotoNav.forEach((item) => {
//         personPhotoNav[item].classList.add("active");
//         item.addEventListener('click', console.log ("hello"))
//     });
// }


function personality (){
    let personPhotoNav = document.querySelectorAll(".person__photo");
    // console.log(personalityNav);
    let peopleQuotePersonal = document.querySelectorAll(".people__quote_personal");
    let personName = document.querySelectorAll(".person__name");
    let personJobTitle = document.querySelectorAll(".person__job_title");
    let photoImgQuote = document.querySelectorAll(".photo_img_quote");
    personPhotoNav.forEach(item => {
        item.addEventListener("click", selectQuoteTab)
    });
    function selectQuoteTab(){
        // console.log("bingo");
        let quoteTabName = null;
        personPhotoNav.forEach((item)=>{
            item.classList.remove("active");
        })
        this.classList.add("active");
        quoteTabName = this.getAttribute("data-tab-name");
        // console.log(quoteTabName, typeof quoteTabName);
        selectQuoteContent(quoteTabName);
    }
    function selectQuoteContent(quoteTabName){
        peopleQuotePersonal.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        personName.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        personJobTitle.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        photoImgQuote.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
    }
}
personality();



