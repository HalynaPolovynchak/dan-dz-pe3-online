// "use strict";

// $('.grid').masonry({
//     itemSelector: '.grid-item',
//     columnWidth: 160
// });

let $container = $('#container');
// Инициализация Масонри, после загрузки изображений
$container.imagesLoaded( function() {
    $container.masonry();
});

let container = document.querySelector('#container');
let msnry = new Masonry( container, {
    // Настройки
    columnWidth: 400,
    itemSelector: '.grid-item'
});

