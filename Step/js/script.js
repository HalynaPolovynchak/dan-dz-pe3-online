"use strict";

// <------------- SECTION__SERVICES ---------------->

let services = function (){
    let tabNav = document.querySelectorAll(".tabs__title");
    let tabContent = document.querySelectorAll(".tab");
    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav)
    });
    function selectTabNav(){
        // let tabName;
        tabNav.forEach(item => {
            item.classList.remove("active");
        })
        this.classList.add("active");
        let tabName = this.getAttribute("data-tab-name");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item => {
            item.classList.contains(tabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        })
    }
};
services();


// <------------- SECTION__WORK ---------------->

let tabWork = function (){
    let tabNav = document.querySelectorAll(".tabs__title__work");
    let tabContent = document.querySelectorAll(".tab__img");
    let tabNameChoice = null;

    tabNav.forEach(item => {
        if (item.classList.contains("active")) {
            tabNameChoice = item.getAttribute("data-tab-name");
        }
        item.addEventListener("click", selectTabNav);

    })

    function selectTabNav(){
        tabNav.forEach(item => {
            item.classList.remove("active");
        })
        this.classList.add("active");
        let tabName = this.getAttribute("data-tab-name");
        tabNameChoice = tabName;
        selectTabContent(tabName);
    }

    function selectTabContent(tabName){
        tabContent.forEach(item => {
            item.classList.contains(tabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        })
    }

    const btnWork = document.getElementById("button__work");
    let clickCountWork = 0;

    btnWork.addEventListener("click", showAddWorkImage);
    function showAddWorkImage(){
        document.querySelector(".button_work_loaded").classList.add("active");
        document.querySelector(".plus_work").classList.add("active");
        document.querySelector(".paragraph_load1").classList.add("active");
        setTimeout(()=> {
            document.querySelector(".button_work_loaded").classList.remove("active");
            document.querySelector(".plus_work").classList.remove("active");
            document.querySelector(".paragraph_load1").classList.remove("active");
            clickCountWork++;

            if (clickCountWork === 1) {
                tabContent.forEach(item => {
                    if (item.classList.contains("img__hide")) {
                        item.classList.add("active");
                        item.classList.remove("img__hide");
                    }
                })
                selectTabContent(tabNameChoice);
            }
            if (clickCountWork === 2) {
                tabContent.forEach(item => {
                    if (item.classList.contains("img__hide2")) {
                        item.classList.add("active");
                        item.classList.remove("img__hide2");
                    }
                })
                    btnWork.innerHTML = "";
                    document.querySelector(".work__container").classList.add("active");
                selectTabContent(tabNameChoice);
            }
        },2000);

    }

};
tabWork();


// <------------- SECTION__PEOPLE ---------------->

const rightBtn = document.getElementById("right_button");
rightBtn.addEventListener("click", sliderLeft);

const leftBtn = document.getElementById("left_button");
leftBtn.addEventListener("click", sliderRight);

let slidesPersons = [...document.querySelectorAll(".person__photo")];
let stepClickBtn = 0;
let activeIndex = 0;

function sliderLeft(){
    let appendChildElem = slidesPersons[stepClickBtn];
    document.querySelector('#sliderMini').appendChild(appendChildElem);

    if ((stepClickBtn + 1) === slidesPersons.length) {
        stepClickBtn = 0;
    } else {
        stepClickBtn++;
    }

    const photoActive = slidesPersons.find((item, index) => {
        const isActive = item.classList.contains("active");
        if (!isActive) return ;
        activeIndex = index;
        return item;
    })
    photoActive.classList.remove("active");

    if(activeIndex + 1 < slidesPersons.length){
        slidesPersons[activeIndex + 1].classList.add("active");
        findActiveContent(slidesPersons[activeIndex + 1]);
    } else {
        slidesPersons[activeIndex-(slidesPersons.length-1)].classList.add("active");
        findActiveContent(slidesPersons[activeIndex-(slidesPersons.length-1)]);
    }
}

function sliderRight() {
    let removeChild;
    if (stepClickBtn === 0){
        removeChild = slidesPersons[slidesPersons.length - 1];
    } else {
        removeChild = slidesPersons[stepClickBtn - 1];
    }

    if(stepClickBtn-1 === -1){
        stepClickBtn = slidesPersons.length - 1;
    } else {
        stepClickBtn--;
    }

    document.querySelector('#sliderMini').removeChild(removeChild);
    document.querySelector('#sliderMini').insertAdjacentElement("afterbegin", removeChild);

    const photoActive = slidesPersons.find((item, index) => {
        const isActive = item.classList.contains("active");
        if (!isActive) return ;
        activeIndex = index;
        return item;
    })
    photoActive.classList.remove("active");

    if (activeIndex - 1 === -1) {
        slidesPersons[slidesPersons.length - 1].classList.add("active");
        findActiveContent(slidesPersons[slidesPersons.length - 1]);
    } else {
        slidesPersons[activeIndex - 1].classList.add("active");
        findActiveContent(slidesPersons[activeIndex - 1]);
    }
}


//function for choice content in container

function findActiveContent(paramIncoming){
    let personPhotoNav = document.querySelectorAll(".person__photo");
    let peopleQuotePersonal = document.querySelectorAll(".people__quote_personal");
    let personName = document.querySelectorAll(".person__name");
    let personJobTitle = document.querySelectorAll(".person__job_title");
    let photoImgQuote = document.querySelectorAll(".photo_img_quote");

    selectQuoteTab();
    function selectQuoteTab(){
        let quoteTabName = null;

        personPhotoNav.forEach((item)=>{
            item.classList.remove("active");
        })
        paramIncoming.classList.add("active");
        quoteTabName = paramIncoming.getAttribute("data-tab-name");
        selectQuoteContent(quoteTabName);
    }

    function selectQuoteContent(quoteTabName){
        peopleQuotePersonal.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        personName.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        personJobTitle.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
        photoImgQuote.forEach(item => {
            item.classList.contains(quoteTabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        });
    }
}

function personality (){
    slidesPersons.forEach(item => {
        item.addEventListener("click", () => {
            let indexItem = slidesPersons.indexOf(item);
            findActiveContent(slidesPersons[indexItem]);
        })
    });
}
personality();

// <------------- SECTION__GALLERY---------------->

const $container = $('#container');
// Инициализация Масонри, после загрузки изображений
$container.imagesLoaded( function() {
    $container.masonry();
})

const msnry = new Masonry( '.grid', {
    columnWidth: 200,
    itemSelector: '.grid-item',
});

let galleryGridBig = function (){
    let tabBig = document.querySelectorAll(".gallery__big");
    tabBig.forEach(item => {
        item.addEventListener("click", selectImgBig)
    });
    let imgBig;
    let imgChange;
    let imgContentSrcParent;
    function selectImgBig(){
        imgContentSrcParent = this.parentElement.getAttribute("data-img-src");

        imgChange = document.querySelector(".gallery__item_big").lastElementChild;
        let imgChangeSrc = imgChange.getAttribute("src");
        imgChangeSrc = imgContentSrcParent;
        imgChange.srcset = imgChangeSrc;

        imgBig = document.querySelector(".back__gallery_img");
        imgBig.classList.add("active");
    }
    function closeImg(){
        let close = document.querySelector(".span_gallery");
        close.addEventListener("click", () => {
            imgBig.classList.remove("active");
        })
    }
    closeImg();
}
galleryGridBig();

let galleryBtnAddImg = function (){
    let gallery_btn = document.getElementById("gallery_btn")
    gallery_btn.addEventListener("click", showMoreGalleryImg);

    function showMoreGalleryImg(){
        document.querySelector(".button_gallery_loaded").classList.add("active");
        document.querySelector(".plus_gallery").classList.add("active");
        document.querySelector(".paragraph_load2").classList.add("active");

        setTimeout(() => {
            document.querySelector(".button_gallery_loaded").classList.remove("active");
            document.querySelector(".plus_gallery").classList.remove("active");
            document.querySelector(".paragraph_load2").classList.remove("active");
            let arrayGalleryHide = document.querySelectorAll('.grid-item_hide');
            arrayGalleryHide.forEach((item)=> {
                item.classList.remove("grid-item_hide");
            })
            //  <---------   Чтоб не ехали картинки   ------------->
            const $container = $('#container');
            $container.imagesLoaded( function() {
                $container.masonry();
            })

            const msnry = new Masonry( '.grid', {
                columnWidth: 200,
                itemSelector: '.grid-item',
            });
            //  <--------------------------------------------------->
            document.querySelector(".gallery__grid").classList.add("active");
            gallery_btn.innerHTML = "";
        }, 2000);
    }

}
galleryBtnAddImg();
