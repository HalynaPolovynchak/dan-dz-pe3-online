"use strict";
const student = {
    name: "John",
    lastName: "MacKinley",
    age: 17,
    father: "Robert",
    mother: {
        name: "Helena",
        lastName: "Shields",
        age: 48,
        father: {
            name: "Edward",
            lastName: "Shields",
            age: 72,
        },
        bankValue: [5000, 20000, 1800],
        job: "Operator",
    },
    tabStudent: [12, 15, 18, 10],
};

const cloneStudent = deepClone(student);
function deepClone(object) {
    const cloneObj = {};
    for (const i in object) {
        if (object[i] instanceof Object) {
            cloneObj[i] = deepClone(object[i]);
            continue;
        }
        cloneObj[i] = object[i];
    }
    return cloneObj;
}

// проверка глубокого клонирования
console.log(student);
console.log(cloneStudent);
student.name = "Stiven";
student.mother.name = "Melisa";
student.mother.bankValue = [85000,15000];
console.log(cloneStudent);
cloneStudent.mother.father.name = "Ret";




