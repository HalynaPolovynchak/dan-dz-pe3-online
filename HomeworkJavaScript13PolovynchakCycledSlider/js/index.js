"use strict";
let slides = document.querySelectorAll(".image-to-show"); //получаем коллекцию элементов изображений
let slider = []; //создаем вспомогательный пустой массив, в который будем получать src атрибуты изображений
for (let i = 0; i < slides.length; i++){
    slider[i] = slides[i].src;
    slides[i].remove(); //Очищаем нашу коллекцию, удаляя каждый после использования со страницы
}
const startBtn = document.getElementById("start");
const stopBtn = document.getElementById("stop");
let timer = 3000; //Чтоб не менять каждый раз в интервалах и обратном отсчете
let timerHideId;
let timerMls = null; // задаем , чтоб пройти валидацию в обратном отсчете
let second2 = timer;
let step = 0; // Шаг номера картинки
let offset = 0; // Смещение изображения

function showImg () {
    let img = document.createElement("img");
    if (img.src !== null){
        document.getElementById("slide").innerHTML = "";
    }
    img.src = slider[step];
    img.classList.add('image-to-show');

    img.style.left = offset*400 + 'px'; // получаем картинку и интегрируем ее внутрь блока с учетом смещения на размер картинки
    document.querySelector('#slide').appendChild(img);
    if (step + 1 === slider.length) { //проверяем если в блоке последняя картинка степ нужно обнулить,
        step = 0;
    } else {
        step++;
    }

    timerHideId = setTimeout(() => {
                img.classList.toggle('image-to-show');
                img.classList.toggle('image-to-hide');
            }, timer - 500);

    if (!timerMls) {
        timerMls = setInterval(counting, 40); // шаг 40, т.к. при 1 идет задержка
        function counting () {
            if (second2 > 40){ //проверка достаточности для шага таймера
                second2 = second2 - 40;
            } else second2 = timer; // снова начинаем с 3 секунд
            document.getElementById("backTimer").innerHTML =`0${(second2/1000).toFixed(0)}`+" : "+ `${Math.floor((second2/100))}`;
        }
    }
    startBtn.setAttribute('disabled', ''); //пока не выполнится хотя бы раз функция старт не активный,
                                                            // иначе какой смысл в этой кнопке
}

showImg(); //запустили функцию
let timerId = setInterval(showImg, timer); //начинаем циклично показывать картинки каждые 3 сек

//кнопка остановки, нужно вернуть активность кнопке старт, нужно остановить timerId, timerMls и timerDeleteId,
// иначе зацикливается и шаг увеличивается на кол-во этих сетов. На всякий случай обновить счетчик до 3 сек
stopBtn.addEventListener("click", () => {
    startBtn.removeAttribute('disabled');
    clearInterval(timerId); // останавливаем дальнейшее выполнение функции, иначе через 3 сек снова выполнится
    clearInterval(timerMls); // остановим таймер обратного отсчета
    clearTimeout(timerHideId); // остановим - не даст выполнить timerHideId и картинка не исчезнет
    timerMls = null;
    second2 = timer;
});
//кнопка возобновить. Нужно задать атрибут disabled чтоб нельзя было после нажатия еще несколько раз нажать, потому что собъется
//timerId уже остановлен кнопкой стоп - clearInterval(timerId);
startBtn.addEventListener("click", () => {
    timerId = setInterval(showImg, timer);
    startBtn.setAttribute('disabled', ''); // хоть в функции это и предусмотрено но на всякий случай
});








// let timerDeleteId; // пришлось вывести в глобальную область, не знаю почему, но иначе не работает
// //нужно удалить предыдущий, нужно попробовать еще через фрагмент, т.к. при возобновлении сохраняется предыдущий
//     timerDeleteId = setTimeout(deleteImg, timer); //Для удаления картинок из нашего дива
//     function deleteImg(){
//         document.querySelector('#slide').removeChild(img);
//     }